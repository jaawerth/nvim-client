; metadata manipulation macros

; used by metadata macros to tell whether metadata is enabled
(local meta-enabled (pcall _SCOPE.specials.doc
                           (list (sym :doc) (sym :doc)) _SCOPE _CHUNK))
(fn meta/when-enabled [...]
  "Execute body in an implicit `do` when metadata is enabled. Otherwise,
contents are excluded from compiled output. Always returns nil."
  (when meta-enabled `(do ,...)))

(λ meta/with [func ...]
  "Accepts and always returns func. When metadata is enabled, evaluates body 
in an implicit `let` with the global metadata table bound to `$metadata`. If 
body returns a table, it's set as the metadata of func. The $metadata api is:

Get/set entire metadata for a function:
  (. $metadata some-func)                         ; get func's metadata tbl
  (tset $metadata func all-metadata-for-somefunc) ; set func's metadata tbl
  ($metadata:get func key)                        ; get meta field for func
  ($metadata:set func key value)                  ; set meta field for func
  ($metadata:setall func key1 val1 key2 val2 ...) ; set metadata k/v pairs

The built-in metadata keys are :fnl/docstring and :fnl/arglist."
  (if (not meta-enabled) func
    (let [meta-sym (sym :$metadata)]
      `(let [func#     ,func
             ,meta-sym (. (require :fennel) :metadata)]
         (local fn-meta# (do ,...))
         (when (= :table (type fn-meta#))
           (each [k# v# (pairs fn-meta#)]
             (: ,meta-sym :set func# k# v#)))
         func#))))

{: meta/with : meta/when-enabled }
