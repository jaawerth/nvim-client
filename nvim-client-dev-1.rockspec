package = "nvim-client"
version = "dev-1"
source = {
   url = "git+ssh://git@gitlab.com/jaawerth/nvim-client.git"
}
description = {
   homepage = "https://gitlab.com/jaawerth/nvim-client",
   license = "MIT"
}
build = {
   type = "builtin",
   modules = {}
}
dependencies = {
  "fennel ~> 0.7",
  "nvim-client ~> 0.2",
}
