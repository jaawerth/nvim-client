# nvim-client for fennel (and lua!)

A friendly wrapper around [neovim/lua-client](https://github.com/neovim/lua-client) for neovim
[fennel](https://fennel-lang.org) repl adventures.
You know, just in case you ever wanted to remote control your editor from a Lisp REPL.

More README to come; this was split off from a tinkering project with the intent to flesh it out
a bit and publish as a package. Documentation and configuration are still minimal at present.
Still, it works!

This module builds up an API that very closely mirrors neovim's own Lua API, so it should feel
comfortable. Plus, docstrings for all the nvim methods are right at your fingertips.

## screenshot

![repl screenhot](screenshot.png "thar be repl")
