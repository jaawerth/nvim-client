(require-macros :macros)
(local (mpack Session Tcp Socket) (values (require :mpack)
                                          (require :nvim.session)
                                          (require :nvim.tcp_stream)
                                          (require :nvim.socket_stream)))
(local (fmt join) (values string.format table.concat))

; an api.mpack file needs to be generated from /path/to/neovim/scripts/vim_gendoc.py
; which only exists in the neovim nightlies for 0.5.0
(fn get-api-doc [mpack-path]
  (let [fh         (io.open (or mpack-path (os.getenv :NVIM_APIDOC_MPACK) "./nvim-docs/api.mpack"))
        (r-ok raw) (when fh (pcall fh.read fh "*a"))
        (ok docs)  (when r-ok (pcall mpack.unpack raw))]
    (when ok docs)))

(λ mk-api-func [sess fn-info ?api-doc]
  (meta/with (partial sess:request fn-info.name)
             (local meta {:fnl/arglist      []
                          :fnl/docstring    (if (not ?api-doc) ""
                                                (join ?api-doc.doc "\n"))
                          :nvim/name        fn-info.name
                          :nvim/method?     fn-info.method
                          :nvim/return-type fn-info.return_type
                          })
             (when fn-info.parameters
               (local sig [])
               (each [i [type-inf arg] (ipairs fn-info.parameters)]
                 (tset meta.fnl/arglist i arg)
                 (tset sig i (.. arg ":" type-inf)))
               (tset meta :fnl/docstring
                     (fmt "%s\n\nnvim api call: %s%s" meta.fnl/docstring
                          fn-info.name
                          (match (join sig " ") "" ""
                            s (.. "\nSignature: [" s "]")))))

             meta))

(fn mk-client [conn-info]
  "Connect to an nvim instance and build api functions via nvim_get_api_info
conn-info can be one of the following:
* {: host : port} - connect to nvim over a TCP socket
* {: socket }     - connect to nvim over a unix socket

See `:help listen` and `:help $NVIM_LISTEN_ADDRESS` in nvim for how to
listen on an open socket."
  (let [api-doc (get-api-doc)
        conn (match conn-info
               {: host : port} (Tcp.open host port)
               {: socket} (Socket.open socket)
               _ (error "connection requires {: host : port} or {: socket}"))
        session (Session.new conn)
        [api-info-n api-info] (match (session:request "nvim_get_api_info")
                                (true info) info
                                (_ ?msg) (error (.. "Error getting api info from nvim:\n"
                                                    (tostring ?msg))))
        client {: conn : session :api []}]
    ;; build local client.api from api-info
    (each [_ v (ipairs api-info.functions)]
      (when (not v.deprecated_since)
        (local f (mk-api-func session v (-?> api-doc (. v.name))))
        (tset client.api (v.name:gsub "^nvim_" "") f)))
    client))

{:connect mk-client}
